class CreateEncomendas < ActiveRecord::Migration
  def change
    create_table :encomendas do |t|
      t.string :endereco
      t.string :cartao_credito

      t.timestamps
    end
  end
end
