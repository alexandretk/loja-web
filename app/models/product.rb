class Product < ActiveRecord::Base

belongs_to :category

has_attached_file :image, :styles => { :small  => "150x150>", :medium  => "300x300>"},
				  :url => "/assets/images/:id/:style/:basename.:extension",
				  :path => ":rails_root/public/assets/images/:id/:style/:basename.:extension"



#do_not_validate_attachment_file_type :image
validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
#validates_attachment_presence :image
#validates_attachment_size :image, :less_than => 5.megabytes
#validates_attachment_content_type :image, :content_type => ['image/jpeg', 'image/png']

validates :description, :name, :price, :presence=> true
validates :price, :numericality => true

end

#,
#				  :url => "/assets/images/:id/:style/:basename.:extension",
#				  :path => ":rails_root/public/assets/images/:id/:style/:basename.:extension"

