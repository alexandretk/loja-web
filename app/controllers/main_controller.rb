class MainController < ApplicationController
  def index
  	@products = Product.order(:name)
    @categorias = Category.order(:name)
  end

  def show
  	@product = Product.find(params[:id])
  end

  def search 

  end 

  def search_results
    #@products = Product.where(:name => params[:keywords])
    @products = Product.where("name LIKE ?", "%#{params[:keywords]}%")
  end
    def categoria
    @products = Product.where(:category_id => params[:categoria])
  end

end
