class EncomendasController < InheritedResources::Base

	before_filter :authenticate_normal_user!


  private

    def encomenda_params
      params.require(:encomenda).permit(:endereco, :cartao_credito, :comprador)
    end
end

