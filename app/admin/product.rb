ActiveAdmin.register Product do


  index do
    column :name
    column :category
    column :description
    column :created_at
    column :price, :sortable => :price do |product|
      number_to_currency product.price
    end  
    actions
    end


    show do |product|
      attributes_table do
        row :name
        row :category
        row :description
        row :price
        row :created_at
        row :updated_at
        row :image do
          image_tag(product.image.url(:medium))
        end
      end
      active_admin_comments
    end






  permit_params   :category_id, :name, :category, :price, :description, :image


  form :html => { :multipart=>true } do |f|
      f.inputs "Product" do
      f.input :category
      f.input :name
      f.input :price
      f.input :description
      f.input :image, :as => :file, :hint => f.template.image_tag(f.object.image.url(:medium))
    #  f.input :image, :as => :file, :hint => f.template.image_tag(f.image.url(:small))
 #    f.submit 
  end
      f.actions
    end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
