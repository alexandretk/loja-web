json.array!(@encomendas) do |encomenda|
  json.extract! encomenda, :id, :endereco, :cartao_credito
  json.url encomenda_url(encomenda, format: :json)
end
